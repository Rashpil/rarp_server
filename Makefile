compiler	=gcc
compflags	=-g -Wall -pthread
path		=bin
obj			=	$(path)/rarp_server.o

default : checkdir $(path)/rarp_server

checkdir :
	@if [ ! -d $(path) ]; then mkdir $(path); fi

$(path)/rarp_server : $(obj)
	$(compiler) $(compflags) -o $(path)/rarp_server $(obj)

$(path)/%.o : %.c
	$(compiler) $(compflags) -c $< -o $@

clean :
	rm -f $(path)/*
