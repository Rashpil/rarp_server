
#include <arpa/inet.h>

#include <linux/if_packet.h>
#include <linux/ip.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>

#include <net/if.h>

#include <netinet/in.h>
#include <netinet/ether.h>
#include <netinet/if_ether.h>
#include <netinet/in_systm.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

//---------------------------- Дефайны -----------------------------------------

#define ETHER_TYPE 0x8035 // 0x0008 - IPv4 0x3580 - RARP 0x0608 - ARP
#define OP_REQUEST 0x0003
#define OP_RESPONSE 0x0004
#define IFNAME "enp4s0f1"
#define MAXPACKET 60
#define ETH_HDR_LEN 14
#define MAX_IP 254
#define IP_LEN 4
#define MAC_LEN 6
#define IP_CHAR_LEN 16
#define OCT_IP_CHAR_LEN 4
#define EMPTY 0;
#define NO_EMPTY 1;

//--------------------------- Структуры ----------------------------------------
typedef struct { //Структура, в которой установлено отношение IP и MAC
	unsigned char ip_addr[IP_LEN];
	unsigned char hd_addr[MAC_LEN];
	int empty;
} MASS_IP;

struct ethernet { // Структура, содержащая ETH заголовок
	unsigned char dest[MAC_LEN];
	unsigned char source[MAC_LEN];
	uint16_t eth_type;
};

struct arp { // Структура, содержащая ARP заголовок
	uint16_t htype;
	uint16_t ptype;
	unsigned char hlen;
	unsigned char plen;
	uint16_t oper;
	unsigned char sender_ha[MAC_LEN];
	unsigned char sender_pa[IP_LEN];
	unsigned char target_ha[MAC_LEN];
	unsigned char target_pa[IP_LEN];
};

//-------------- Функции -------------------------------------------------------
uint8_t *allocate_ustrmem(int len) { // Выделение памяти
	void *tmp;

	if(len <= 0) {
		exit(EXIT_FAILURE);
	}

	tmp = (uint8_t *)malloc(len * sizeof (uint8_t));
	if(tmp != NULL) {
		memset(tmp, 0, len * sizeof (uint8_t));
		return (tmp);
	} else {
		exit(EXIT_FAILURE);
	}
}

void getIP(MASS_IP * *ip, struct arp *my) { // Функция установления соответствия IP и MAC

	for(int i = 0; i < MAX_IP; i++) {
		if(((*ip)[i].hd_addr[0] == (*my).target_ha[0] && (*ip)[i].hd_addr[1] == (*my).target_ha[1] &&
		    (*ip)[i].hd_addr[2] == (*my).target_ha[2] && (*ip)[i].hd_addr[3] == (*my).target_ha[3] &&
		    (*ip)[i].hd_addr[4] == (*my).target_ha[4] && (*ip)[i].hd_addr[5] == (*my).target_ha[5] ) || (*ip)[i].empty == 0) {
			for(int j = 0; j < IP_LEN; j++) {
				(*my).target_pa[j] = (*ip)[i].ip_addr[j];
			}
			for(int j = 0; j < MAC_LEN; j++) {
				(*ip)[i].hd_addr[j] = (*my).target_ha[j];
			}
			(*ip)[i].empty = NO_EMPTY;
			return;
		}
	}
}

//--------------------------- Глобальные переменные ----------------------------


//----------------- Главная функция --------------------------------------------
int main(int argc, char const *argv[]) {

//--------------Инициализация: начало-------------------------------------------
	// Объявления переменных, выделение памяти инициализация переменных
	MASS_IP *data_ip;
	data_ip = malloc(MAX_IP);
	int sockopt = 1;
	char ifName[IFNAMSIZ] = IFNAME;
	uint8_t *ether_frame = allocate_ustrmem(MAXPACKET);
	struct ethernet *eth_hdr = NULL;
	struct arp *arp_hdr = NULL;
	struct ifreq if_mac, if_ip, if_idx;
	struct sockaddr_ll socket_address;
	memset(&socket_address, 0, sizeof(socket_address));
	memset(&if_mac, 0, sizeof(struct ifreq));
	memset(&if_ip, 0, sizeof(struct ifreq));
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ - 1);
	strncpy(if_ip.ifr_name, ifName, IFNAMSIZ - 1);
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ - 1);
	int nosock;
	char ip[IP_CHAR_LEN];
	unsigned char my_ip[IP_LEN];
	char buf_ip[OCT_IP_CHAR_LEN];
	int num = 0;
	// Подключаем сокет и настраиваем его
	if((nosock = socket(PF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1) {
		printf("%s\n", strerror(errno));
		return -1;
	}

	if(setsockopt(nosock, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) == -1) {
		printf("%s\n", strerror(errno));
		close(nosock);
		return -1;
	}
	if(setsockopt(nosock, SOL_SOCKET, SO_BINDTODEVICE, ifName, IFNAMSIZ - 1) == -1) {
		printf("%s\n", strerror(errno));
		close(nosock);
		return -1;
	}
	// Получаем нужные данные о интерфейсе
	ioctl(nosock, SIOCGIFHWADDR, &if_mac);
	ioctl(nosock, SIOCGIFADDR, &if_ip);
	ioctl(nosock, SIOCGIFINDEX, &if_idx);

	strcpy(ip, inet_ntoa(((struct sockaddr_in *)(&if_ip.ifr_ifru.ifru_addr))->sin_addr));
	// Получаем свой IP
	for(int i = 0, j = 0, z = 0; i < IP_CHAR_LEN; i++, j++) {
		if(ip[i] == '.' || i == IP_CHAR_LEN - 1) {
			buf_ip[j] = '\0';
			my_ip[z] = (unsigned char)(atoi(buf_ip));
			z++;
			j = 0;
			i++;
		}
		buf_ip[j] = ip[i];
	}
	//Инициализация структуры с MASS_IP
	for(int i = 0; i < MAX_IP; i++) {
		data_ip[i].ip_addr[0] = my_ip[0];
		data_ip[i].ip_addr[1] = my_ip[1];
		data_ip[i].ip_addr[2] = my_ip[2];
		data_ip[i].ip_addr[3] = i + 1;
		data_ip[i].empty = EMPTY;
	}

//--------------Инициализация: конец--------------------------------------------

//--------------Основной цикл: начало-------------------------------------------
	while(1) {
		//Ждем RARP пакет
		if((num = recv(nosock, ether_frame, MAXPACKET, 0)) <= 0) {
			printf("%s\n", strerror(errno));
			//continue;
		}else {
			//Делим пришедший пакет на заголовки
			eth_hdr = (struct ethernet *)ether_frame;
			arp_hdr = (struct arp *)(ether_frame + ETH_HDR_LEN);
			// Смотрим RARP запрос это или нет
			if( OP_REQUEST == ntohs(arp_hdr->oper)) {
				// Здесь формируем обратное сообщение
				for(int i = 0; i < MAC_LEN; i++) {
					socket_address.sll_addr[i] = arp_hdr->target_ha[i] = eth_hdr->source[i];
					arp_hdr->sender_ha[i] = if_mac.ifr_ifru.ifru_hwaddr.sa_data[i];
				}
				for(int i = 0; i < MAC_LEN; i++) {
					eth_hdr->dest[i] = eth_hdr->source[i];
					eth_hdr->source[i] = arp_hdr->sender_ha[i];
				}
				for(int i = 0; i < IP_LEN; i++) {
					arp_hdr->sender_pa[i] = my_ip[i];
				}
				arp_hdr->oper = htons(OP_RESPONSE);
				//Выдаем IP адрес
				getIP(&data_ip, arp_hdr);
				//Здесь указываем по какому сокету отправлять
				socket_address.sll_family = PF_PACKET;
				socket_address.sll_ifindex = if_idx.ifr_ifindex;
				socket_address.sll_protocol = htons(ETHER_TYPE);
				printf("MAC: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x IP: %d.%d.%d.%d\n", arp_hdr->target_ha[0],arp_hdr->target_ha[1],arp_hdr->target_ha[2],arp_hdr->target_ha[3],arp_hdr->target_ha[4],arp_hdr->target_ha[5], arp_hdr->target_pa[0], arp_hdr->target_pa[1], arp_hdr->target_pa[2], arp_hdr->target_pa[3]);
				//Отправляем RARP ответ
				if(sendto(nosock, ether_frame, MAXPACKET, 0, (struct sockaddr *)&socket_address, sizeof(struct sockaddr_ll)) < 0) {
					printf("%s\n", strerror(errno));
				}
			}
		}
	}
//--------------Основной цикл: конец--------------------------------------------

	return 0;
}
